
package com.itver.server_proyect.database;

import java.util.List;

public interface DAO <T, K>{
    
    boolean insert(T u);
    void update(T u);
    void delete(T u);
    T get(String s1, String s2);
    List<T> getAll();
}
