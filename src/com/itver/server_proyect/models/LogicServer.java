package com.itver.server_proyect.models;

import com.itver.proyect.resources.NetConnection;
import com.itver.proyect.resources.PacketNet;
import com.itver.proyect.resources.Protocol;
import com.itver.proyect.resources.User;
import com.itver.server_proyect.database.MysqlUserDAO;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogicServer {

    private MysqlUserDAO manager;
    private final ArrayList<Session> users_list;

    public LogicServer(MysqlUserDAO manager) {
        this.manager = manager;
        users_list = new ArrayList<>();
    }

    //Metodo para verificar si el usuario esta en la base de datos y autenticarse
    public boolean verifyUserOnDatabase(PacketNet packet, NetConnection connection) {
        User current_user = packet.getUsuario(); //usuario enviado por el cliente
        String usuario = current_user.getUsuario(); //extraccion del usuario 
        String password = current_user.getPassword(); //extraccion de la contraseña
        User userFromDB = manager.get(usuario, password);//Verificar si en la base de datos existe
        if (userFromDB != null) {
            //iniciar la sesion del jugador
            Session session = new Session(userFromDB, connection, users_list.size());
            users_list.add(session);
            session.start();
            return true;
        }
        return false;
    }

    //Metodo para registrar un nuevo usuario en la base de datos
    public boolean registerUserOnDatabase(PacketNet packet, NetConnection connection) {
        User new_user = packet.getUsuario(); //Usuario enviado por el cliente
        if (manager.insert(new_user)) { //insercion en la base de datos del nuevo usuario
            //iniciar sesion del jugador
            Session session = new Session(new_user, connection, users_list.size());
            users_list.add(session);
            session.start();
            return true;
        }

        return false;
    }

    /*private synchronized int agregarUserAEspera(Session sesion) {
        int idx = waiting_users.size();
        waiting_users.add(sesion);
        return idx;
    }*/

 /*    private static final int STATUS_STAND_BY = 0;
    private static final int STATUS_WAITING = 1;
    private static final int STATUS_PENDING = 2;
    private static final int STATUS_PLAYING = 3;*/
    enum Status {
        STAND_BY, WAITING, PENDING, PLAYING
    }

    private synchronized Session getRival(int self) {
        if (users_list.size() < 2) {
            return null;
        }
        Session s = users_list.get(self), r;
        int rival = self;
        do {
            rival = (int) (Math.random() * users_list.size());
        } while (rival == self);
        r = users_list.get(rival);
        System.out.println("Rival: " + r.getUser().getUsuario() + " estatus: " + r.getStatus());
        if (r.getStatus().equals(Status.WAITING)) {
            r.setRival(s);
            return r;
        }
        return null;
    }

    private synchronized void removerSesion(int idx) {
        users_list.remove(idx);
    }

    //Clase que acturara de hilo para las sesiones de los juegadores(clientes)
    public class Session extends Thread {

        private Session rival;
        private final User usuario;
        private final NetConnection individual_connection;
        private Status status;
        private final int idx;
        private int tiro;

        public Session(User user, NetConnection connection, int idx) {
            this.usuario = user;
            this.individual_connection = connection;
            status = Status.STAND_BY;
            this.idx = idx;
            this.tiro = -1;
        }

        @Override
        public void run() {
            System.out.println("Sesion iniciada para el jugador : " + usuario.getUsuario());
            while (!individual_connection.socket.isClosed()) {
                PacketNet received_packet = individual_connection.readPacket();
                if (received_packet == null) {
                    individual_connection.closeComunication();
                    System.out.println("Sesion terminada del usuario: " + usuario.getUsuario());
                }

                if (received_packet.getTipo_mensaje() == null) {
                    System.out.println("NADA");
                    continue;
                }
                PacketNet paquete = new PacketNet();
                paquete.setUsuario(usuario);
                if (received_packet.getTipo_mensaje().equals(Protocol.PLAY)) {
                    System.out.println("YA RECIBÍ EL PEDO");
                    status = Status.WAITING;
                    tiro = -1;
                    rival = null;
                    esperarContrincante(idx);
                    status = Status.PENDING;
                    paquete.setTipo_mensaje(Protocol.REQUEST_ACEPTED);
                    paquete.setMensaje("JugadorEncontrado," + rival.getUser().getUsuario());
                    //System.out.println("Mandando paquetuco: " + paquete.getMensaje());
                    individual_connection.sendPacket(paquete);
                } else if (received_packet.getTipo_mensaje().equals(Protocol.ROUND)) {
                    status = Status.PLAYING;
                    String msg = received_packet.getMensaje().split(",")[1];
                    tiro = Integer.parseInt(msg);
                    //System.out.println("Recibí tiro: " + received_packet.getMensaje());
                    esperarTiroRival();
                    //System.out.println("Esperé el tiro rival: " + rival.getTiro());
                    paquete.setTipo_mensaje(Protocol.ROUND);
                    paquete.setMensaje("Tiro," + rival.getTiro());
                    //System.out.println("Mandando paquetuco2: " + paquete.getMensaje());
                    individual_connection.sendPacket(paquete);
                    status = Status.STAND_BY;
                }
            }
            removerSesion(idx);
        }

        public User getUser() {
            return usuario;
        }

        public Status getStatus() {
            return status;
        }

        public int getTiro() {
            return tiro;
        }

        public void setRival(Session rival) {
            this.rival = rival;
        }

        private void esperarTiroRival() {
            while (rival.getTiro() == -1) {
                System.out.println(usuario.getUsuario() + " está esperando el tiro... ");
            }

        }

        private void esperarContrincante(int self) {
            while (rival == null) {
                rival = getRival(self);
            }
            System.out.println(usuario.getUsuario() + "... ya encontró jugador!");
            ///quitarEspera(this);
        }

    }

}
